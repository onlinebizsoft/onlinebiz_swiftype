<?php

class OnlineBiz_Swiftype_Helper_Producthelper extends Mage_Core_Helper_Abstract
{
	protected static $_currencies;
	
    protected function getFields($store)
    {
        /** @var Mage_Tax_Helper_Data $tax_helper */
        $tax_helper = Mage::helper('tax');

        if ($tax_helper->getPriceDisplayType($store) == Mage_Tax_Model_Config::DISPLAY_TYPE_EXCLUDING_TAX) {
            return array('price' => false);
        }

        if ($tax_helper->getPriceDisplayType($store) == Mage_Tax_Model_Config::DISPLAY_TYPE_INCLUDING_TAX) {
            return array('price' => true);
        }

        return array('price' => false, 'price_with_tax' => true);
    }

    protected function formatPrice($price, $includeContainer, $currency_code)
    {
        /** @var Mage_Directory_Model_Currency $directoryCurrency */
        $directoryCurrency = Mage::getModel('directory/currency');

        if (!isset(static::$_currencies[$currency_code])) {
            static::$_currencies[$currency_code] = $directoryCurrency->load($currency_code);
        }

        /** @var Mage_Directory_Model_Currency $currency */
        $currency = static::$_currencies[$currency_code];

        if ($currency) {
            return $currency->format($price, array(), $includeContainer);
        }

        return $price;
    }
	
	public function getProductCollectionQuery($storeId, $productIds = null, $only_visible = true, $withoutData = false)
    {
        /** @var $products Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection */
        $products = Mage::getResourceModel('catalog/product_collection');

        $products = $products->setStoreId($storeId);
        $products = $products->addStoreFilter($storeId);

        if ($productIds && count($productIds) > 0) {
            $products = $products->addAttributeToFilter('entity_id', array('in' => $productIds));
        }

        if ($withoutData === false) {
            $products = $products
                ->addAttributeToSelect('special_from_date')
                ->addAttributeToSelect('special_to_date')
                ->addAttributeToSelect('visibility')
                ->addAttributeToSelect('status')
                ->addAttributeToSelect('price')
				->addAttributeToSelect('msrp');

            $products->addFinalPrice();
        }
        return $products;
    }

    public function handlePrice(Mage_Catalog_Model_Product &$product)
    {
        $customData = array();
		$fields = $this->getFields($product->getStore());
        $store = $product->getStore();
        $type = $product->getTypeId();
				
        /** @var Mage_Directory_Model_Currency $directoryCurrency */
        $directoryCurrency = Mage::getModel('directory/currency');
        $currencies = $directoryCurrency->getConfigAllowCurrencies();

        $baseCurrencyCode = $store->getBaseCurrencyCode();
		print_r($store->getData);die;
        $groups = array();
		
        /** @var Mage_Tax_Helper_Data $taxHelper */
        $taxHelper = Mage::helper('tax');

        /** @var Mage_Directory_Helper_Data $directoryHelper */
        $directoryHelper = Mage::helper('directory');

        foreach ($fields as $field => $with_tax) {
            $customData[$field] = array();

            foreach ($currencies as $currency_code) {
                $customData[$field][$currency_code] = array();

                $price = (double) $taxHelper->getPrice($product, $product->getPrice(), $with_tax, null, null, null, $product->getStore(), null);
                $price = $directoryHelper->currencyConvert($price, $baseCurrencyCode, $currency_code);

                $_msrpPrice = (double) $taxHelper->getPrice($product, $product->getMsrp(), $with_tax, null, null, null, $product->getStore(), null);
                $_msrpPrice = $directoryHelper->currencyConvert($_msrpPrice, $baseCurrencyCode, $currency_code);
				
				if($_msrpPrice > $price){
					$customData[$field][$currency_code]['default'] = $_msrpPrice;
					$customData[$field][$currency_code]['default_formated'] = $this->formatPrice($_msrpPrice, false, $currency_code);
				}
				else{
					$customData[$field][$currency_code]['default'] = $price;
					$customData[$field][$currency_code]['default_formated'] = $this->formatPrice($price, false, $currency_code);
				}

                $special_price = (double) $taxHelper->getPrice($product, $product->getFinalPrice(), $with_tax, null, null, null, $product->getStore(), null);
                $special_price = $directoryHelper->currencyConvert($special_price, $baseCurrencyCode, $currency_code);
				
				$customData[$field][$currency_code]['special_from_date'] = strtotime($product->getSpecialFromDate());
                $customData[$field][$currency_code]['special_to_date'] = strtotime($product->getSpecialToDate());

                if ($special_price && $special_price < $customData[$field][$currency_code]['default']) {
					$customData[$field][$currency_code]['default_original_formated'] = $customData[$field][$currency_code]['default_formated'];

					$customData[$field][$currency_code]['default'] = $special_price;
					$customData[$field][$currency_code]['default_formated'] = $this->formatPrice($special_price,
						false, $currency_code);
				}
            }
        }
		return $customData;
    }
}
