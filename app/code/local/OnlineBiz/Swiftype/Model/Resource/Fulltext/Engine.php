<?php

class OnlineBiz_Swiftype_Model_Resource_Fulltext_Engine
    extends Mage_CatalogSearch_Model_Resource_Fulltext_Engine
{
    private $_documentFieldTypes = array();

    /**
     * Create/update Documents in Swiftype
     *
     * @param int $storeId
     * @param array $entityIndexes
     * @param string $entity
     * @return \OnlineBiz_Swiftype_Model_Resource_Fulltext_Engine
     */
    final public function saveEntityIndexes($storeId, $entityIndexes, $entity = 'product')
    {	
        if ($entity == 'product') {            
            $bulkCreateOrUpdate = new stdClass();
            $bulkCreateOrUpdate->auth_token = $this->_getHelper()->getAuthToken($storeId);

            foreach ($entityIndexes as $entityId => $index) {
                $document = new stdClass();
                $document->external_id = $entityId;

                foreach ($index as $attributeCode => $value) {
                    $document->fields[] = array(
                        'name' => $attributeCode,
                        'value' => $value,
                        'type' => $this->_getDocumentFieldType($attributeCode)
                    );
                }
                $document->fields[] = array(
                    'name' => 'product_id',
                    'value' => $entityId,
                    'type' => OnlineBiz_Swiftype_Helper_Data::DOCUMENT_FIELD_TYPE_INTEGER
                );
				
				$_product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($entityId);				
				
				$imageHelper = Mage::helper('swiftype/image');				
				$type_img = $this->_getHelper()->getSearchStoreConfig('type', $storeId);
				if($_product->getData($type_img) != '' && $_product->getData($type_img) != 'no_selection' && file_exists(Mage::getBaseDir('media').'/catalog/product'.$_product->getData($type_img))){
					$image = $imageHelper->init($_product, $type_img)->resize($this->_getHelper()->getSearchStoreConfig('width', $storeId), $this->_getHelper()->getSearchStoreConfig('height', $storeId));
					try {
						$image_url = $image->toString();
					} catch (\Exception $e) {
						$image_url = '';
					}
				}
				else{
					$image_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'onlinebizsoft/swiftype/'.$this->_getHelper()->getSearchStoreConfig('image_placeholder', $storeId);
				}
				
				$document->fields[] = array(
                    'name' => 'image',
                    'value' => $image_url,
                    'type' => OnlineBiz_Swiftype_Helper_Data::DOCUMENT_FIELD_TYPE_STRING
                );
				$price = 0;
				$special_price = 0;
				$min_price = 0;
				$view_min_price = '';
				$max_price = 0;
				$view_max_price = '';
				$view_price = '';
				$view_special_price = '';
				if($_product->getTypeId() == 'simple'){
					$_msrpPrice = $_product->getMsrp();
					$price_default = $_product->getPrice();
					if($_msrpPrice > $price_default){
						$price = $_msrpPrice;
					}
					else{
						$price = $price_default;
					}
					$special_price_check = $_product->getFinalPrice();
					if ($special_price_check && $special_price_check < $price) {
						$special_price = $price;
						$price = $special_price_check;
						$view_special_price = Mage::helper('core')->currency($special_price,true,false);
					}
					$view_price = Mage::helper('core')->currency($price,true,false);
				}
				elseif($_product->getTypeId() == 'grouped'){
					$ids = $_product->getTypeInstance(true)->getChildrenIds($_product->getId());
					$ids = call_user_func_array('array_merge', $ids);
					if(count($ids) > 0){
						$min = PHP_INT_MAX;
						$max = 0;
						$products = Mage::getResourceModel('catalog/product_collection')
								->setStoreId($storeId)
								->addStoreFilter($storeId)
								->addAttributeToFilter('entity_id', array('in' => $ids))->addFinalPrice();
						if(count($products) > 0){							
							foreach ($products as $sub_product) {
								$price_min_max_fixed = $sub_product->getFinalPrice();
								
                                $min = min($min, $price_min_max_fixed);
                                $max = max($max, $price_min_max_fixed);
							}
						}
						else{
							$min = $max;
						}
						if ($min != $max) {
							$price = $min;
							$min_price = $min;
							$view_min_price = Mage::helper('core')->currency($min,true,false);
							$view_max_price = Mage::helper('core')->currency($max,true,false);
						}
						else{
							$price = $min;
							$view_price = Mage::helper('core')->currency($min,true,false);
						}
					}
				}
				elseif ($_product->getTypeId() == 'bundle') {
                    $min = PHP_INT_MAX;
                    $max = 0;
					$_priceModel = $_product->getPriceModel();
					list($min, $max) = $_priceModel->getTotalPrices($_product, null, true, true);
					$min = $min;
					$max = $max;
					if ($min != $max) {
						$price = $min;
						$min_price = $min;
						$view_min_price = Mage::helper('core')->currency($min,true,false);
						$view_max_price = Mage::helper('core')->currency($max,true,false);
					}
					else{
						$price = $min;
						$view_price = Mage::helper('core')->currency($min,true,false);
					}
				}
				$document->fields[] = array(
                    'name' => 'url',
                    'value' => $_product->getProductUrl(),
                    'type' => OnlineBiz_Swiftype_Helper_Data::DOCUMENT_FIELD_TYPE_STRING
                );
				$document->fields[] = array(
                    'name' => 'price_formated',
                    'value' => $view_price,
                    'type' => OnlineBiz_Swiftype_Helper_Data::DOCUMENT_FIELD_TYPE_STRING
                );
				$document->fields[] = array(
                    'name' => 'price_original_formated',
                    'value' => $view_special_price,
                    'type' => OnlineBiz_Swiftype_Helper_Data::DOCUMENT_FIELD_TYPE_STRING
                );				
				$document->fields[] = array(
                    'name' => 'price',
                    'value' => $price,
                    'type' => OnlineBiz_Swiftype_Helper_Data::DOCUMENT_FIELD_TYPE_FLOAT
                );
				$document->fields[] = array(
                    'name' => 'special_price',
                    'value' => $special_price,
                    'type' => OnlineBiz_Swiftype_Helper_Data::DOCUMENT_FIELD_TYPE_FLOAT
                );
				$document->fields[] = array(
                    'name' => 'min_price',
                    'value' => $min_price,
                    'type' => OnlineBiz_Swiftype_Helper_Data::DOCUMENT_FIELD_TYPE_FLOAT
                );
				$document->fields[] = array(
                    'name' => 'price_min_formated',
                    'value' => $view_min_price,
                    'type' => OnlineBiz_Swiftype_Helper_Data::DOCUMENT_FIELD_TYPE_STRING
                );
				$document->fields[] = array(
                    'name' => 'price_max_formated',
                    'value' => $view_max_price,
                    'type' => OnlineBiz_Swiftype_Helper_Data::DOCUMENT_FIELD_TYPE_STRING
                );
				
                $bulkCreateOrUpdate->documents[] = $document;
            }

            $swiftypeApiParameters = array(
                'uri' => array(
                    'engines' => $this->_getHelper()->getEngineSlug($storeId),
                    'document_types' => $entity,
                    'documents' => 'bulk_create_or_update_verbose'
                ),
                'raw_data' => array(
                    'enctype' => 'application/json',
                    'data' => Zend_Json::encode($bulkCreateOrUpdate)
                )
            );
            
            $swiftypeApiClient = $this->_getHelper()->getSwiftypeClient($swiftypeApiParameters, $storeId);
            $swiftypeApiResponse = $swiftypeApiClient->request(Zend_Http_Client::POST);
	
            if ($swiftypeApiResponse->getStatus() == 200) {
                $responseMessage = Zend_Json::decode($swiftypeApiResponse->getBody());
                if ($responseMessage[0] !== true) {
                    $this->_logError($storeId, $swiftypeApiParameters, array_keys($entityIndexes), $swiftypeApiResponse);
                }
            } else {
                $this->_logError($storeId, $swiftypeApiParameters, array_keys($entityIndexes), $swiftypeApiResponse);
            }           
        }
        
        return $this;
    }
    
    final protected function _logError($storeId, $swiftypeApiParameters, $productIds, $swiftypeApiResponse)
    {
        Mage::helper('swiftype/log')->logIndexRequest($storeId, $swiftypeApiParameters, $productIds);
        Mage::helper('swiftype/log')->logIndexResponse($storeId, $swiftypeApiParameters, $swiftypeApiResponse);
    }

    /**
     * Get Swiftype Document Field Type for Attribute Code
     *
     * @param string $attributeCode
     * @return string
     */
    final protected function _getDocumentFieldType($attributeCode)
    {
        if (!in_array($attributeCode, $this->_documentFieldTypes)) {
            $entityType = Mage::getModel('eav/entity_type')
                    ->loadByCode(Mage_Catalog_Model_Product::ENTITY);
            /* @var $entityType Mage_Eav_Model_Entity_Type */
            $attribute = Mage::getModel('catalog/resource_eav_attribute')
                    ->loadByCode($entityType->getId(), $attributeCode);
            /* @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */

            if ($attribute->getId()) {
                $documentFieldType = $attribute->getSwiftypeDocumentFieldType();
            } else {
                $documentFieldType = OnlineBiz_Swiftype_Helper_Data::DOCUMENT_FIELD_TYPE_TEXT;
            }
            $this->_documentFieldTypes[$attributeCode] = $documentFieldType;
        }
        return $this->_documentFieldTypes[$attributeCode];
    }

    /**
     * @return OnlineBiz_Swiftype_Helper_Data
     */
    final protected function _getHelper()
    {
        return Mage::helper('swiftype');
    }

    /**
     * Remove Document from Swiftype
     *
     * @param mixed $storeId
     * @param mixed $entityId
     * @param string $entity
     * @return \OnlineBiz_Swiftype_Model_Resource_Fulltext_Engine
     */
    final public function cleanIndex($storeId = null, $entityId = null, $entity = 'product')
    {
        if ($entity == 'product' && $entityId) {
            if (!is_array($entityId)) {
                $swiftypeApiParameters = array(
                    'uri' => array(
                        'engines' => $this->_getHelper()->getEngineSlug($storeId),
                        'document_types' => $entity,
                        'documents' => $entityId
                    ),
                    'get' => array(
                        'auth_token' => $this->_getHelper()->getAuthToken($storeId)
                    )

                );
                $swiftypeApiClient = $this->_getHelper()->getSwiftypeClient($swiftypeApiParameters, $storeId);
                $swiftypeApiResponse = $swiftypeApiClient->request(Zend_Http_Client::DELETE);

                if ($swiftypeApiResponse->getStatus() == 200 || $swiftypeApiResponse->getStatus() == 406) {
                    return $this;
                }

                Mage::helper('swiftype/log')->logDeleteResponse($storeId, $swiftypeApiParameters, $swiftypeApiResponse);
            }
        }
        return $this;
    }

    /**
     *
     * @param array $index Data to index
     * @param string $separator
     * @return array Data formatted ready for indexing
     */
    final public function prepareEntityIndex($index, $separator = ' ')
    {
        $separator = ' ';
        foreach ($index as $attributeCode => $value) {
            if (is_array($value)) {
                reset($value);
                $value = $value[key($value)];
            }
            if (preg_match('/^(Enabled)?$/', $value) || preg_match('/^(in_stock|price)?$/', $attributeCode)) {
                unset($index[$attributeCode]);
            } else {
                $index[$attributeCode] = $value;
            }
        }
        return $index;
    }
}