<?php

class OnlineBiz_Swiftype_Block_System_Config_Form_Field_Sorts extends OnlineBiz_Swiftype_Block_System_Config_Form_Field_AbstractField
{
    public function __construct()
    {
        $this->settings = array(
            'columns' => array(
                'attribute' => array(
                    'label'   => 'Attribute',
                    'options' => function () {
						$store   = Mage::app()->getRequest()->getParam('store');
						if($store)
						{
							$c = Mage::getModel('core/store')->getCollection()->addFieldToFilter('code', $store);  
							$item = $c->getFirstItem();
							$opts = Mage::helper('swiftype')->getProductAdditionalAttributes($item->getStoreId());
							return $opts;
						}
                        return array();
                    },
                    'rowMethod' => 'getAttribute',
                    'width'     => 160,
                ),
                'sort' => array(
                    'label'   => 'Sort',
                    'options' => array(
                        'asc'  => 'Ascending',
                        'desc' => 'Descending',
                    ),
                    'rowMethod' => 'getSort',
                ),
                'label' => array(
                    'label' => 'Label',
                    'style' => 'width: 200px;',
                ),
            ),
            'buttonLabel' => 'Add Sorting Attribute',
            'addAfter'    => false,
        );

        parent::__construct();
    }
}
